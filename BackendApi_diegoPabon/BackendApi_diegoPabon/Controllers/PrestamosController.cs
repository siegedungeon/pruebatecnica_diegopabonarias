﻿using BackendApi_diegoPabon.Models.BooksModel;
using BackendApi_diegoPabon.Models.Context;
using BackendApi_diegoPabon.Models.LogicaDePrestamo;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendApi_diegoPabon.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrestamosController : ControllerBase
    {
        private readonly LibraryContext _context;

        public PrestamosController(LibraryContext context)
        {
            _context = context;
        }

        // POST:
        [HttpPost("Prestamo")]
        public async Task<ActionResult> Prestamo([FromBody] M_PeticionPrestamo obj)
        {

            // se inserta el prestamo en prestamos y se actualiza el libro como no disponible
            var result = _context.Libros.SingleOrDefault(b => b.Id == obj.IdLibro);
            if (result != null)
            {
                try
                {
                    _context.Libros.Attach(result);
                    result.Estado = obj.Operacion;
                    _context.Entry(result).State = EntityState.Modified;

                    var prestamo = new Prestamos()
                    {
                        CodigoEstudiante=obj.CodigoEstudiante,
                        IdLibro=obj.IdLibro,
                        IdUsuario=obj.IdUsuario,
                        NombreEstudiante=obj.NombreEstudiante
                    };

                    _context.Prestamos.Add(prestamo);

                    await _context.SaveChangesAsync();
                    return Ok();
                }
                catch (Exception ex)
                {
                    return BadRequest();
                }
            }
            return BadRequest();
        }

        // get:
        [HttpGet("LibrosDisponibles")]
        public async Task<ActionResult<List<Libro>>> LibrosDisponibles()
        {
            try
            {
                var disponibles = await _context.Libros.Where(a => a.Estado == 1).OrderBy(a => a.Title).ToListAsync();
                return disponibles;
            }
            catch (Exception)
            {

                return BadRequest() ;
            }
           
        }
        // get:
        [HttpGet("LibrosPrestados")]
        public async Task<ActionResult<List<Libro>>> LibrosPrestados()
        {
            try
            {
                var prestados = await _context.Libros.Where(a => a.Estado == 2).OrderBy(a => a.Title).ToListAsync(); ;
                return prestados;
            }
            catch (Exception)
            {

                return BadRequest();
            }

        }

    }
}
