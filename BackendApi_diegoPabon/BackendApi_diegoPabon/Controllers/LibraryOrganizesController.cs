﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BackendApi_diegoPabon.Models.Context;
using BackendApi_diegoPabon.Models.Library;

namespace BackendApi_diegoPabon.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LibraryOrganizesController : ControllerBase
    {
        private readonly LibraryContext _context;

        public LibraryOrganizesController(LibraryContext context)
        {
            _context = context;
        }

        // GET: api/LibraryOrganizes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LibraryOrganize>>> GetLibraryOrganize()
        {
            return await _context.LibraryOrganize.ToListAsync();
        }

        // GET: api/LibraryOrganizes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LibraryOrganize>> GetLibraryOrganize(int id)
        {
            var libraryOrganize = await _context.LibraryOrganize.FindAsync(id);

            if (libraryOrganize == null)
            {
                return NotFound();
            }

            return libraryOrganize;
        }

        // PUT: api/LibraryOrganizes/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLibraryOrganize(int id, LibraryOrganize libraryOrganize)
        {
            if (id != libraryOrganize.IdOrganize)
            {
                return BadRequest();
            }

            _context.Entry(libraryOrganize).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LibraryOrganizeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LibraryOrganizes
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<LibraryOrganize>> PostLibraryOrganize(LibraryOrganize libraryOrganize)
        {
            _context.LibraryOrganize.Add(libraryOrganize);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLibraryOrganize", new { id = libraryOrganize.IdOrganize }, libraryOrganize);
        }

        // DELETE: api/LibraryOrganizes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLibraryOrganize(int id)
        {
            var libraryOrganize = await _context.LibraryOrganize.FindAsync(id);
            if (libraryOrganize == null)
            {
                return NotFound();
            }

            _context.LibraryOrganize.Remove(libraryOrganize);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool LibraryOrganizeExists(int id)
        {
            return _context.LibraryOrganize.Any(e => e.IdOrganize == id);
        }
    }
}
