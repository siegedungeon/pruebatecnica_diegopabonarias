﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BackendApi_diegoPabon.Models.Context;
using BackendApi_diegoPabon.Models.Library;

namespace BackendApi_diegoPabon.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LibraryZonesController : ControllerBase
    {
        private readonly LibraryContext _context;

        public LibraryZonesController(LibraryContext context)
        {
            _context = context;
        }

        // GET: api/LibraryZones
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LibraryZones>>> GetLibraryZones()
        {
            return await _context.LibraryZones.ToListAsync();
        }

        // GET: api/LibraryZones/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LibraryZones>> GetLibraryZones(int id)
        {
            var libraryZones = await _context.LibraryZones.FindAsync(id);

            if (libraryZones == null)
            {
                return NotFound();
            }

            return libraryZones;
        }

        // PUT: api/LibraryZones/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLibraryZones(int id, LibraryZones libraryZones)
        {
            if (id != libraryZones.IdLibraryZone)
            {
                return BadRequest();
            }

            _context.Entry(libraryZones).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LibraryZonesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LibraryZones
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<LibraryZones>> PostLibraryZones(LibraryZones libraryZones)
        {
            _context.LibraryZones.Add(libraryZones);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLibraryZones", new { id = libraryZones.IdLibraryZone }, libraryZones);
        }

        // DELETE: api/LibraryZones/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLibraryZones(int id)
        {
            var libraryZones = await _context.LibraryZones.FindAsync(id);
            if (libraryZones == null)
            {
                return NotFound();
            }

            _context.LibraryZones.Remove(libraryZones);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool LibraryZonesExists(int id)
        {
            return _context.LibraryZones.Any(e => e.IdLibraryZone == id);
        }
    }
}
