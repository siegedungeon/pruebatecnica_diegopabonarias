﻿namespace BackendApi_diegoPabon.Models.LogicaDePrestamo
{
    public class M_PeticionPrestamo
    {
        public int IdLibro { get; set; }
        public int IdUsuario { get; set; }
        public long CodigoEstudiante { get; set; }
        public string NombreEstudiante { get; set; }
        public int Operacion { get; set; }
    }
}
