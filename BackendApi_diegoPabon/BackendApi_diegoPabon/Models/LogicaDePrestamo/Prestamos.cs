﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendApi_diegoPabon.Models.LogicaDePrestamo
{
    public class Prestamos
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdLibro { get; set; }
        public long CodigoEstudiante { get; set; }
        public string NombreEstudiante { get; set; }
    }
}
