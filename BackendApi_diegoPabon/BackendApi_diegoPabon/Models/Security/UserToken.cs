﻿using BackendApi_diegoPabon.Models.UserModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendApi_diegoPabon.Models.Security
{
    public class UserToken : Usuario
    {
        public string AccessToken { get; set; }

        public UserToken(Usuario user)
        {
            this.Id  = user.Id;
            this.Email = user.Email;
            this.FirstName = user.FirstName;
            this.SecondName = user.SecondName;
            this.FirstSurname = user.FirstSurname;
            this.SecondSurname = user.SecondSurname;
        }
    }
}
