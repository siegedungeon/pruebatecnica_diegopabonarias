﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackendApi_diegoPabon.Models.Library
{
    public class LibraryZones
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdLibraryZone { get; set; }
        [Required]
        [MaxLength(60)]
        public string Nombre { get; set; }


        public virtual ICollection<LibraryOrganize> Organize { get; set; }

    }
}
