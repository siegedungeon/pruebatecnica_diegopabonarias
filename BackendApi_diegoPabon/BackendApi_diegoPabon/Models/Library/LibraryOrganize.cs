﻿using BackendApi_diegoPabon.Models.BooksModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackendApi_diegoPabon.Models.Library
{
    public class LibraryOrganize
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdOrganize { get; set; }
        [Required]
        public int IdLibraryZone { get; set; }
        [Required]
        public int ISBN { get; set; }


        public virtual Libro Libros { get; set; }
        public virtual LibraryZones Zonas { get; set; }
    }
}
