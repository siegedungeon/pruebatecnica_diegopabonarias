﻿using BackendApi_diegoPabon.Models.BooksModel;
using BackendApi_diegoPabon.Models.Library;
using BackendApi_diegoPabon.Models.LogicaDePrestamo;
using BackendApi_diegoPabon.Models.UserModels;
using Microsoft.EntityFrameworkCore;

namespace BackendApi_diegoPabon.Models.Context
{
    public class LibraryContext : DbContext
    {

        public LibraryContext()
        {
        }

        public LibraryContext(DbContextOptions<LibraryContext> options)
            : base(options)
        {
        }



        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<Libro> Libros { get; set; }
        public virtual DbSet<Autor> Autores { get; set; }
        public virtual DbSet<Editorial> Editoriales { get; set; }
        public virtual DbSet<LibraryOrganize> LibraryOrganize { get; set; }
        public virtual DbSet<LibraryZones> LibraryZones { get; set; }
        public virtual DbSet<Prestamos> Prestamos { get; set; }

    }
}
