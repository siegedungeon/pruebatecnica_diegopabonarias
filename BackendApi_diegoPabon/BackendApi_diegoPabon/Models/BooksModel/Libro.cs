﻿using BackendApi_diegoPabon.Models.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackendApi_diegoPabon.Models.BooksModel
{
    public class Libro
    {

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public int ISBN { get; set; }
        [Required]
        [MaxLength(60)]
        public string Title { get; set; }
        [Required]
        [MaxLength(60)]
        public string Type { get; set; }
        [Required]
        public int PubId { get; set; }
        [Required]
        public int AutorId { get; set; }
        [Required]
        [MaxLength(250)]
        public string Notes { get; set; }
        [Required]
        public DateTime PublishedDate { get; set; }

        //Estado de Activo o dado de baja 1 activo, 0 dado de baja 2 prestado
        public int Estado { get; set; }
        [Required]
        [MaxLength(250)]
        public string EstadoEstetico { get; set; }


        public virtual Editorial Editorial { get; set; }
        public virtual Autor Autores { get; set; }

        public virtual ICollection<LibraryOrganize> Organize { get; set; }
    }
}
