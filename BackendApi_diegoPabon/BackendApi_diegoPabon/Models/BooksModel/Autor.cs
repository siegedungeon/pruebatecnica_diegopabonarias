﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackendApi_diegoPabon.Models.BooksModel
{
    public class Autor
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AuthorId { get; set; }
        [Required]
        [MaxLength(60)]
        public string LastName { get; set; }
        [Required]
        [MaxLength(60)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(40)]
        public string City { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [MaxLength(150)]
        public string EmailAddress { get; set; }
    }
}
