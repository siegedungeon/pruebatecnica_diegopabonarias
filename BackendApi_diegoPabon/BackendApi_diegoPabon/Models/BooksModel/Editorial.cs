﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BackendApi_diegoPabon.Models.BooksModel
{
    public class Editorial
    {
        public Editorial()
        {
            Libros = new HashSet<Libro>();
        }

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PubId { get; set; }
        [Required]
        [MaxLength(60)]
        public string PublisherName { get; set; }
        [Required]
        [MaxLength(60)]
        public string City { get; set; }
        [Required]
        [MaxLength(60)]
        public string State { get; set; }
        [Required]
        [MaxLength(60)]
        public string Country { get; set; }

        public virtual ICollection<Libro> Libros { get; set; }
    }
}
