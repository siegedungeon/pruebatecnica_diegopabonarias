﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace BackendApi_diegoPabon.Models.UserModels
{
    public class Usuario
    {
        // modelo de usuario basico para empleados

        [Key]
        [Required]
        [ConcurrencyCheck]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

       
        [Required]
        [MaxLength(150)]
        public string FirstName { get; set; }

        [MaxLength(150)]
        public string SecondName { get; set; }

        [Required]
        [MaxLength(150)]
        public string FirstSurname { get; set; }

        [MaxLength(150)]
        public string SecondSurname { get; set; }

        [Required]
        public DateTime Fecha_Nacimiento { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [MaxLength(150)]
        public string Email { get; set; }
       

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
 

        public DateTime FechaRegistro { get; set; } = DateTime.Now;
    }

    public class M_Login {


        [Required]
        [DataType(DataType.EmailAddress)]
        [MaxLength(150)]
        public string Email { get; set; }


        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

    }

}

