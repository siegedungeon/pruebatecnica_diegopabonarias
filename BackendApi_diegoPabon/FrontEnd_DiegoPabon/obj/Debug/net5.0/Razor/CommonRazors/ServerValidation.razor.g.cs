#pragma checksum "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\CommonRazors\ServerValidation.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0fd30698041108d8edabf09dab63cf177942abff"
// <auto-generated/>
#pragma warning disable 1591
namespace FrontEnd_DiegoPabon.CommonRazors
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\_Imports.razor"
using FrontEnd_DiegoPabon;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\_Imports.razor"
using FrontEnd_DiegoPabon.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\_Imports.razor"
using BlazorPro.Spinkit;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\_Imports.razor"
using FrontEnd_DiegoPabon.Models.Autenticacion;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\_Imports.razor"
using FrontEnd_DiegoPabon.Services;

#line default
#line hidden
#nullable disable
    public partial class ServerValidation : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
#nullable restore
#line 1 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\CommonRazors\ServerValidation.razor"
 if (IsVisible)
{

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\CommonRazors\ServerValidation.razor"
 if (Result)
{

#line default
#line hidden
#nullable disable
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "id", "divValidationMessage");
            __builder.AddAttribute(2, "class", "col-10 alert alert-danger row");
            __builder.OpenElement(3, "div");
            __builder.AddAttribute(4, "class", "col-11");
            __builder.AddAttribute(5, "style", "text-align:left");
            __builder.OpenElement(6, "span");
            __builder.AddContent(7, 
#nullable restore
#line 7 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\CommonRazors\ServerValidation.razor"
               ChildContent

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(8, " has been saved successfully");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(9, "\n    ");
            __builder.OpenElement(10, "div");
            __builder.AddAttribute(11, "class", "col-1");
            __builder.AddAttribute(12, "style", "text-align:right");
            __builder.OpenElement(13, "a");
            __builder.AddAttribute(14, "style", "text-underline-position:below; cursor:pointer");
            __builder.AddAttribute(15, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 10 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\CommonRazors\ServerValidation.razor"
                                                                           (() => CloseValidation())

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(16, "x");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 12 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\CommonRazors\ServerValidation.razor"
       }
                else
                {

#line default
#line hidden
#nullable disable
            __builder.OpenElement(17, "div");
            __builder.AddAttribute(18, "id", "divValidationMessage");
            __builder.AddAttribute(19, "class", "col-10 alert alert-danger row");
            __builder.AddMarkupContent(20, "<div class=\"col-11\" style=\"text-align:left\"><span style=\"font-style:italic; font-weight:bold\">Oops, something went wrong. Please contact system admin.</span></div>\n    ");
            __builder.OpenElement(21, "div");
            __builder.AddAttribute(22, "class", "col-1");
            __builder.AddAttribute(23, "style", "text-align:right");
            __builder.OpenElement(24, "a");
            __builder.AddAttribute(25, "style", "text-underline-position:below; cursor:pointer");
            __builder.AddAttribute(26, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 20 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\CommonRazors\ServerValidation.razor"
                                                                           (() => CloseValidation())

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(27, "x");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.CloseElement();
#nullable restore
#line 22 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\CommonRazors\ServerValidation.razor"
      }

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\CommonRazors\ServerValidation.razor"
       }

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
#nullable restore
#line 24 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\CommonRazors\ServerValidation.razor"
        [Parameter]
            public bool IsVisible { get; set; }

            [Parameter]
            public bool Result { get; set; }

            [Parameter]
            public RenderFragment ChildContent { get; set; }

            protected override async Task OnParametersSetAsync()
            {
                //Result = false; ///authorService.CheckConnection();
                //IsVisible = true;
                await base.OnParametersSetAsync();
            }

            private void CloseValidation()
            {
                IsVisible = false;
            } 

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
