#pragma checksum "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\Login\_Imports.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "37619a785d8ce37a2bfa30f3dc3455dfe010199f"
// <auto-generated/>
#pragma warning disable 1591
namespace FrontEnd_DiegoPabon.Pages.Login
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using FrontEnd_DiegoPabon;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using BlazorPro.Spinkit;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using FrontEnd_DiegoPabon.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using FrontEnd_DiegoPabon.Models.BooksModel;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using FrontEnd_DiegoPabon.Services.Library;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Blazored.LocalStorage;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using FrontEnd_DiegoPabon.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using FrontEnd_DiegoPabon.Models.LogicaDePrestamo;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\Login\_Imports.razor"
using FrontEnd_DiegoPabon.Models.Autenticacion;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\Login\_Imports.razor"
using FrontEnd_DiegoPabon.Models.UserModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\Login\_Imports.razor"
using Newtonsoft.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\Login\_Imports.razor"
using FrontEnd_DiegoPabon.Services.Usuarios;

#line default
#line hidden
#nullable disable
    public partial class _Imports : System.Object
    {
        #pragma warning disable 1998
        protected void Execute()
        {
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
