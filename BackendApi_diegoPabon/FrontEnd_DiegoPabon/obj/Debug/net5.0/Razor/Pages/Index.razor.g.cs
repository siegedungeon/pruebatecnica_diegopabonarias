#pragma checksum "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\Index.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f4c04fbd547c090440b28eec22cd483dda002325"
// <auto-generated/>
#pragma warning disable 1591
namespace FrontEnd_DiegoPabon.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using FrontEnd_DiegoPabon;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using BlazorPro.Spinkit;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using FrontEnd_DiegoPabon.Models.Autenticacion;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using FrontEnd_DiegoPabon.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using FrontEnd_DiegoPabon.Models.BooksModel;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using FrontEnd_DiegoPabon.Services.Library;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using FrontEnd_DiegoPabon.Services.Usuarios;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using FrontEnd_DiegoPabon.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\_Imports.razor"
using FrontEnd_DiegoPabon.Models.LogicaDePrestamo;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\Index.razor"
using FrontEnd_DiegoPabon.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\Index.razor"
using Blazored.LocalStorage;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\Index.razor"
using System.Security.Claims;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/")]
    public partial class Index : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Authorization.AuthorizeView>(0);
            __builder.AddAttribute(1, "Authorized", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Authorization.AuthenticationState>)((context) => (__builder2) => {
                __builder2.OpenElement(2, "h3");
                __builder2.OpenElement(3, "b");
                __builder2.AddContent(4, 
#nullable restore
#line 12 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\Index.razor"
                user.Identity.Name

#line default
#line hidden
#nullable disable
                );
                __builder2.CloseElement();
                __builder2.AddContent(5, " Hola!!!");
                __builder2.CloseElement();
            }
            ));
            __builder.AddAttribute(6, "NotAuthorized", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Authorization.AuthenticationState>)((context) => (__builder2) => {
                __builder2.AddMarkupContent(7, "<h3><b>Bienvenido a nuestra Libreria</b></h3>");
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 22 "D:\PRACTIVA REPOS__\PruebaTecnica\DiegoP_Initial\BackendApi_diegoPabon\FrontEnd_DiegoPabon\Pages\Index.razor"
        [CascadingParameter]
    private Task<AuthenticationState> authenticationStateTask { get; set; }
    ClaimsPrincipal user;

    bool IsUserAuthenticated;

    protected override async Task OnInitializedAsync()
    {
       
        user = (await authenticationStateTask).User;

        if (user.Identity.IsAuthenticated)
            IsUserAuthenticated = true;

    } 

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IAuthorizationService authorizationService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ILocalStorageService localStorageService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager navigationManager { get; set; }
    }
}
#pragma warning restore 1591
