
using Blazored.LocalStorage;
using FrontEnd_DiegoPabon.Helpers;
using FrontEnd_DiegoPabon.Models;
using FrontEnd_DiegoPabon.Models.Autenticacion;
using FrontEnd_DiegoPabon.Models.BooksModel;
using FrontEnd_DiegoPabon.Models.Library;
using FrontEnd_DiegoPabon.Models.LogicaDePrestamo;
using FrontEnd_DiegoPabon.Services.Library;
using FrontEnd_DiegoPabon.Services.Usuarios;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Net.Http;

namespace FrontEnd_DiegoPabon
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();

            var appSettingSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingSection);

            services.AddTransient<ValidateHeaderHandler>();

            services.AddScoped<AuthenticationStateProvider, CustomAutenticationStateProvider>();

            services.AddBlazoredLocalStorage();
            services.AddAuthorization();
            services.AddHttpClient<IUserService, UserService>();

            services.AddHttpClient<ILibrary<Autor>, Library<Autor>>()
                    .AddHttpMessageHandler<ValidateHeaderHandler>();
            services.AddHttpClient<ILibrary<Editorial>, Library<Editorial>>()
                   .AddHttpMessageHandler<ValidateHeaderHandler>();
            services.AddHttpClient<ILibrary<Libro>, Library<Libro>>()
                   .AddHttpMessageHandler<ValidateHeaderHandler>();
            services.AddHttpClient<ILibrary<LibraryOrganize>, Library<LibraryOrganize>>()
                  .AddHttpMessageHandler<ValidateHeaderHandler>();
            services.AddHttpClient<ILibrary<LibraryZones>, Library<LibraryZones>>()
                  .AddHttpMessageHandler<ValidateHeaderHandler>();
            services.AddHttpClient<ILibrary<M_PeticionPrestamo>, Library<M_PeticionPrestamo>>()
                  .AddHttpMessageHandler<ValidateHeaderHandler>();


            services.AddSingleton<HttpClient>();

           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
