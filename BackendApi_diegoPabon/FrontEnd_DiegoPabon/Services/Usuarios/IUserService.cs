﻿using System.Threading.Tasks;
using FrontEnd_DiegoPabon.Models.UserModels;

namespace FrontEnd_DiegoPabon.Services.Usuarios
{
    public interface IUserService
    {
        public Task<M_Usuario> LoginAsync(M_Usuario user);
        public Task<M_Usuario> RegisterUserAsync(M_Usuario user);
        public Task<M_Usuario> GetUserByAccessTokenAsync(string accessToken);
    }
}
