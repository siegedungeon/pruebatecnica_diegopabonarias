﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEnd_DiegoPabon.Pages
{
    public interface IErrorComponent
    {
        void ShowError(string title, string message);
    }
}
