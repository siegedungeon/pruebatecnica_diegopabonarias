﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEnd_DiegoPabon.Models
{
    public class AppSettings
    {
        public string BookStoresBaseAddress { get; set; }
    }
}