﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FrontEnd_DiegoPabon.Models.Library
{
    public class LibraryZones
    {
       
        public int IdLibraryZone { get; set; }
        
        public string Nombre { get; set; }


        public virtual ICollection<LibraryOrganize> Organize { get; set; }

    }
}
