﻿using FrontEnd_DiegoPabon.Models.BooksModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FrontEnd_DiegoPabon.Models.Library
{
    public class LibraryOrganize
    {
     
        public int IdOrganize { get; set; }
        [Required]
        public int IdLibraryZone { get; set; }
        [Required]
        public int ISBN { get; set; }


        public virtual Libro Libros { get; set; }
        public virtual LibraryZones Zonas { get; set; }
    }
}
