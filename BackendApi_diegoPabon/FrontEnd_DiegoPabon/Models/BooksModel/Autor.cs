﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FrontEnd_DiegoPabon.Models.BooksModel
{
    public class Autor
    {
       
        public int AuthorId { get; set; }
        [Required(ErrorMessage = "Los Apellidos son obligatorios")]
        [StringLength(60, ErrorMessage = "Los Apellidos  no puede tener mas de 40 caracteres")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Los Nombres son obligatorios")]
        [StringLength(40, ErrorMessage = "Los nombres  no puede tener mas de 40 caracteres")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "La Ciudad es obligatoria")]
        [StringLength(40, ErrorMessage = "La Ciudad no puede tener mas de 40 caracteres")]
        public string City { get; set; }

        [Required(ErrorMessage = "El Correo es obligatorio")]
        [DataType(DataType.EmailAddress)]
        [StringLength(150, ErrorMessage = "El Correo no puede tener mas de 150 caracteres")]
        public string EmailAddress { get; set; }


        public Autor()
        {

        }
        public Autor(int authorId, string firstName, string lastName, string emailAddress,
                     string city)
        {
            AuthorId = authorId;
            FirstName = firstName;
            LastName = lastName;
            EmailAddress = emailAddress;
            City = city;
        }

        public void clear()
        {
            AuthorId = 0;
            FirstName = "";
            LastName = "";
            EmailAddress = "";
            City = "";
        }
    }
}
