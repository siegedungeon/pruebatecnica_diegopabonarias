﻿using FrontEnd_DiegoPabon.Models.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FrontEnd_DiegoPabon.Models.BooksModel
{
    public class Libro
    {

        public int Id { get; set; }


        [Required(ErrorMessage = "El Pais es obligatorio")]
        [Range(0, 999999, ErrorMessage = "ISBN debe estar entre 0 y 999999")]
        public int ISBN { get; set; }


        [Required(ErrorMessage = "El titulo es obligatorio")]
        [StringLength(60, ErrorMessage = "El titulo  no puede tener mas de 60 caracteres")]
        public string Title { get; set; }

        [Required(ErrorMessage = "El Tipo es obligatorio")]
        [StringLength(60, ErrorMessage = "El Tipo  no puede tener mas de 60 caracteres")]
        public string Type { get; set; }

        [Required]
        public int PubId { get; set; }


        [Required]
        public int AutorId { get; set; }


        [Required(ErrorMessage = "las Notes es obligatorio")]
        [StringLength(250, ErrorMessage = "las Notes  no puede tener mas de 250 caracteres")]
        public string Notes { get; set; }

        [Required(ErrorMessage = " PublishedDate es obligatorio")]
        public DateTime PublishedDate { get; set; }

        //Estado de Activo o dado de baja 1 activo, 0 dado de baja 2 prestado
        [Required(ErrorMessage = " PublishedDate es obligatorio")]
        [Range(0, 2, ErrorMessage = "Estado debe estar entre 0 y 2")]
        public int Estado { get; set; }

        [Required(ErrorMessage = "El EstadoEstetico es obligatorio")]
        [StringLength(60, ErrorMessage = "El EstadoEstetico  no puede tener mas de 60 caracteres")]
        public string EstadoEstetico { get; set; }


        public virtual Editorial Editorial { get; set; }
        public virtual Autor Autores { get; set; }

        public virtual ICollection<LibraryOrganize> Organize { get; set; }
    }
}
