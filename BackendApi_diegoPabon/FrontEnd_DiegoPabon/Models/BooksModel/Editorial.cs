﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEnd_DiegoPabon.Models.BooksModel
{
    public class Editorial
    {
       
        public int PubId { get; set; }
        [Required(ErrorMessage = "PublisherName es obligatorio")]
        [StringLength(60, ErrorMessage = "PublisherName  no puede tener mas de 40 caracteres")]
        public string PublisherName { get; set; }

        [Required(ErrorMessage = "la ciudad es obligatorio")]
        [StringLength(60, ErrorMessage = "la ciudad  no puede tener mas de 40 caracteres")]
        public string City { get; set; }

        [Required(ErrorMessage = "El departamento es obligatorio")]
        [StringLength(60, ErrorMessage = "El departamento  no puede tener mas de 40 caracteres")]
        public string State { get; set; }

        [Required(ErrorMessage = "El Pais es obligatorio")]
        [StringLength(60, ErrorMessage = "El Pais  no puede tener mas de 40 caracteres")]
        public string Country { get; set; }

        public virtual ICollection<Libro> Libros { get; set; }
    }
}
