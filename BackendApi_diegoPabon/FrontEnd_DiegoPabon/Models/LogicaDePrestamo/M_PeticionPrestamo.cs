﻿using System.ComponentModel.DataAnnotations;

namespace FrontEnd_DiegoPabon.Models.LogicaDePrestamo
{
    public class M_PeticionPrestamo
    {
        [Required(ErrorMessage = "El IdLibro es obligatorio")]
        [Range(0, 999999, ErrorMessage = "IdLibro debe estar entre 0 y 999999")]
        public int IdLibro { get; set; }

        [Required(ErrorMessage = "El IdUsuario es obligatorio")]
        [Range(0, 999999, ErrorMessage = "IdUsuario debe estar entre 0 y 999999")]
        public int IdUsuario { get; set; }

        [Required(ErrorMessage = "El CodigoEstudiante es obligatorio")]
        [Range(0, 999999, ErrorMessage = "CodigoEstudiante debe estar entre 0 y 999999")]
        public long CodigoEstudiante { get; set; }

        [Required(ErrorMessage = "El NombreEstudiante es obligatorio")]
        [StringLength(150, ErrorMessage = "El titulo  no puede tener mas de 60 caracteres")]
        public string NombreEstudiante { get; set; }

        public int Operacion { get; set; }
    }
}
