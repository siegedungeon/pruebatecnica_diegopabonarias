﻿using FrontEnd_DiegoPabon.Models.UserModels;

namespace FrontEnd_DiegoPabon.Models.Security
{
    public class UserToken : M_Usuario
    {
        public string AccessToken { get; set; }

        public UserToken(M_Usuario user)
        {
            this.Id  = user.Id;
            this.Email = user.Email;
            this.FirstName = user.FirstName;
            this.SecondName = user.SecondName;
            this.FirstSurname = user.FirstSurname;
            this.SecondSurname = user.SecondSurname;
        }
    }
}
