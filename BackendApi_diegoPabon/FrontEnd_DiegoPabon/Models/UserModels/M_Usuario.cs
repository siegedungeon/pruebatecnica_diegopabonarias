﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FrontEnd_DiegoPabon.Models.UserModels
{
    public class M_Usuario
    {
        // modelo de usuario basico para empleados

       
        public int Id { get; set; }


        [Required(ErrorMessage = "El Primer Nombre es obligatorio")]
        [StringLength(60, ErrorMessage = "El Primer Nombre  no puede tener mas de 60 caracteres")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "El Segundo Nombre es obligatorio")]
        [StringLength(60, ErrorMessage = "El Segundo Nombre  no puede tener mas de 60 caracteres")]
        public string SecondName { get; set; }

        [Required(ErrorMessage = "El Primer Apellido es obligatorio")]
        [StringLength(60, ErrorMessage = "El Primer Apellido  no puede tener mas de 60 caracteres")]
        public string FirstSurname { get; set; }

        [Required(ErrorMessage = "El Segundo Apellido es obligatorio")]
        [StringLength(60, ErrorMessage = "El Segundo Apellido  no puede tener mas de 60 caracteres")]
        public string SecondSurname { get; set; }

        [Required]
        public DateTime Fecha_Nacimiento { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [MaxLength(150)]
        public string Email { get; set; }
       

        [JsonIgnore]
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string AccessToken { get; set; }


        public DateTime FechaRegistro { get; set; } = DateTime.Now;
    }

}

